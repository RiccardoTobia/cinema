<%@page import="it.pegaso.cinema.model.Film"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>info</title>
</head>
<body style="color: gold; background-color: DarkBlue;">
	<% Film movie = (Film) request.getAttribute("film");%>
	
	<h1 style="text-align: center; color: gold"><%= movie.getTitolo() %></h1>
	
	<h3>Sinossi: </h3>
	<p><%= movie.getDescrizione() %></p>
	
	<h3>Orario: </h3><p><%= movie.getOraInizio() %>
	
	<a href="AcquistaBiglietti?orario=<%= movie.getOraInizio()%>&posti=<%= movie.getPosti()%>"><input type="button" value="acquista biglietti"></a>
	
</body>
</html>