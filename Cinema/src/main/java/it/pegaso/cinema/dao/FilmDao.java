package it.pegaso.cinema.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.cinema.model.Film;

public class FilmDao {
	
	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/cinema?user=root&password=@First254";
	
	private Connection connection;
	private PreparedStatement getByTitolo;
	
	public Film filmByTitolo(String titolo) {
		Film movie = new Film();
		
		try {
			getGetByTitolo().clearParameters();
			getGetByTitolo().setString(1, titolo);
			
			ResultSet rs = getGetByTitolo().executeQuery();
			
			if(rs.next()) {
				
				movie.setId(rs.getInt("id"));
				movie.setTitolo(titolo);
				movie.setRegista(rs.getString("regista"));
				movie.setGenere(rs.getString("genere"));
				movie.setPosti(rs.getInt("posti"));
				movie.setOraInizio(rs.getTime("oraInizio"));
				movie.setDescrizione(rs.getString("descrizione"));
				
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return movie;
		
	}
	
	public List<Film> getAll(){
		
		List<Film> result = new ArrayList<Film>();
		
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("select * from film");
			
			while(rs.next()) {
				
				Film movie = new Film();
				
				movie.setId(rs.getInt("id"));
				movie.setTitolo(rs.getString("titolo"));
				movie.setRegista(rs.getString("regista"));
				movie.setGenere(rs.getString("genere"));
				movie.setPosti(rs.getInt("posti"));
				movie.setOraInizio(rs.getTime("oraInizio"));
				movie.setDescrizione(rs.getString("descrizione"));
				
				result.add(movie);
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		if(connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
		}
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}


	public PreparedStatement getGetByTitolo() throws ClassNotFoundException, SQLException {
		if(getByTitolo == null) {
			getByTitolo = getConnection().prepareStatement("select * from film where titolo=?");
		}
		return getByTitolo;
	}


	public void setGetByTitolo(PreparedStatement getByTitolo) {
		this.getByTitolo = getByTitolo;
	}
	
	
}
